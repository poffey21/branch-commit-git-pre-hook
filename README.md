# Branch Commit Git Pre Hook

As a user, it is nice to have Git recommend a branch creation rather than on master (replicating GitLab's Web IDE functionality)

## Global Installation

```bash
# Global installation instructions: (requires git 2.9 or later)
# NOTE: if you configure core.hooksPath, then git only runs the hooks 
#       from that directory (and ignores repo-specific hooks in .git/hooks/), 
#       but these pre-commit hooks contain a block at the end which executes 
#       a repo-specific pre-commit hook if it's present. It's a small hax 
#       that I think is pretty nice.
mkdir $HOME/.githooks
git config --global core.hooksPath $HOME/.githooks
curl -fL -o $HOME/.githooks/pre-commit https://gitlab.com/poffey21/branch-commit-git-pre-hook/raw/master/pre-commit
chmod +x $HOME/.githooks/pre-commit
```

#### Global Uninstallation

```bash
rm $HOME/.githooks/pre-commit
git config --global --unset core.hooksPath
```

## Local Installation:

```bash
cd path/to/git/repo
curl -fL -o .git/hooks/pre-commit https://gitlab.com/poffey21/branch-commit-git-pre-hook/raw/master/pre-commit
chmod +x .git/hooks/pre-commit
```
#### Local Uninstallation:

```bash
rm .git/hooks/pre-commit
```

### Reference Links

[GitLab Issue](https://gitlab.com/gitlab-org/gitlab/issues/4545)

[Original Author](https://gist.github.com/stefansundin/9059706)
